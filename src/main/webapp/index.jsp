<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!--
        lab1: 輸入不同的內容，觀察網址列的變化
        lab2: method 改成 POST，會發生什麼事，怎麼修改到正常
        lab3: 輸入中文試試看
        lab4: 建立一個新的 servlet，怎麼改變 action 去對應？
        -->
        <form action="form" method="POST"><!--action 用來連接前端(.jsp)和後端(.java),method預設為GET 設為post重整會重複輸出且後端需要將編碼改為"utf-8"-->
            name:<input type="text" name="name" value="" /><br/><!--<br/>換行-->
            a.<input type="checkbox" name="array" value="1" /><br/>
            b.<input type="checkbox" name="array" value="2" /><br/>
            c.<input type="checkbox" name="array" value="3" /><br/>
            <input type="submit" value="Submit" />
            
        </form>
<!--        <form action="process" method="POST">
            name: <input type="text" name="username"></input>
            <input type="submit"/>
        </form>-->
    </body>
</html>
